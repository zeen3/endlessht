#![feature(drain_filter)]
use bytes::*;
use rand::*;
use rand_xoshiro::Xoshiro128Plus as Rng;
use serde::Serialize;
use socket2::*;
use std::io::{self, Result as R, Write as _};
use std::net::*;
use std::time::*;

const HTTP: &'static [u8] = b"HTTP/1.1 200 OK\r\n";
fn main() -> R<()> {
    let mut out = std::io::stdout();
    let mut rng = Rng::from_entropy();
    let mut qu = Vec::with_capacity(32);
    struct SockDat {
        sock: Socket,
        addr: SocketAddr,
        remaining: Bytes,
        connected: Instant,
        on: SocketAddr,
    }
    struct IoErr(io::Error);
    impl Serialize for IoErr {
        fn serialize<S: serde::Serializer>(&self, s: S) -> Result<S::Ok, S::Error> {
            use std::fmt;
            struct Debug<T>(T);
            impl<T: fmt::Debug> fmt::Display for Debug<T> {
                fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
                    write!(f, "{:?}", self.0)
                }
            }
            s.collect_str(&Debug(&self.0))
        }
    }
    #[derive(Serialize)]
    #[serde(rename_all = "snake_case")]
    enum Event {
        Connect {
            from: SocketAddr,
            at: chrono::DateTime<chrono::Utc>,
            to: SocketAddr,
        },
        ErrIo {
            err: IoErr,
        },
        Sent {
            connected: usize,
            disconnected: usize,
            highwatermark: usize,
            at: chrono::DateTime<chrono::Utc>,
        },
        Disconnect {
            from: SocketAddr,
            lasted: Duration,
            #[serde(skip_serializing_if = "Option::is_none")]
            err: Option<IoErr>,
            at: chrono::DateTime<chrono::Utc>,
            on: SocketAddr,
        },
        Bind {
            addrs: Vec<SocketAddr>,
        },
    }
    fn evt<W: std::io::Write>(e: Event, out: &mut W) {
        serde_json::to_writer(out, &e).unwrap();
        println!();
    };
    let mut last = 0;
    let mut highwatermark = 0;
    let mut sockets: Vec<Socket> = [80u16, 3000, 8080]
        .iter()
        .map(|port| -> R<Socket> {
            let sock = Socket::new(Domain::ipv6(), Type::stream(), Some(Protocol::tcp()))?;
            let addr = SocketAddr::new(Ipv6Addr::UNSPECIFIED.into(), *port).into();
            sock.bind(&addr)?;
            sock.set_nodelay(true)?;
            sock.set_nonblocking(true)?;
            sock.set_linger(Some(Duration::from_secs(0)))?;
            sock.set_write_timeout(Some(Duration::from_secs(10)))?;
            sock.listen(128)?;
            Ok(sock)
        })
        .filter_map(Result::ok)
        .collect();
    assert!(sockets.len() > 0, "sockets must be bindable");
    evt(
        Event::Bind {
            addrs: sockets
                .iter()
                .filter_map(|s| s.local_addr().ok())
                .filter_map(|s| s.as_std())
                .collect(),
        },
        &mut out,
    );
    out.flush()?;
    loop {
        'accept: loop {
            for socket in sockets.iter_mut() {
                match socket.accept() {
                    Ok((sock, addr)) => {
                        if let Some(addr) = addr.as_std() {
                            evt(
                                Event::Connect {
                                    from: addr,
                                    at: chrono::Utc::now(),
                                    to: socket.local_addr()?.as_std().unwrap(),
                                },
                                &mut out,
                            );
                            if let Ok(()) = sock.set_nonblocking(true) {
                                qu.push(SockDat {
                                    sock,
                                    addr,
                                    remaining: Bytes::from_static(HTTP),
                                    connected: Instant::now(),
                                    on: socket.local_addr()?.as_std().unwrap(),
                                });
                            }
                        }
                    }
                    Err(e) if e.kind() == io::ErrorKind::WouldBlock => {
                        break 'accept;
                    }
                    Err(e) => {
                        evt(Event::ErrIo { err: IoErr(e) }, &mut out);
                    }
                }
            }
        }
        let mut bytes_next = None;
        let cut = qu
            .drain_filter(|sock| {
                use io::ErrorKind::*;
                if sock.remaining.is_empty() {
                    sock.remaining = bytes_next
                        .get_or_insert_with(|| {
                            let args = format!("X-{:x}: {:x}\r\n", rng.next_u32(), rng.next_u64());
                            let buf = Bytes::from(args);
                            buf
                        })
                        .clone()
                }
                match sock.sock.write(&sock.remaining) {
                    Ok(n) => sock.remaining = sock.remaining.slice(n..),
                    Err(e) => {
                        match e.kind() {
                            WouldBlock => return false,
                            NotConnected | BrokenPipe => {
                                evt(
                                    Event::Disconnect {
                                        from: sock.addr,
                                        at: chrono::Utc::now(),
                                        lasted: sock.connected.elapsed(),
                                        err: None,
                                        on: sock.on,
                                    },
                                    &mut out,
                                );
                            }
                            _ => {
                                evt(
                                    Event::Disconnect {
                                        from: sock.addr,
                                        at: chrono::Utc::now(),
                                        lasted: sock.connected.elapsed(),
                                        err: IoErr(e).into(),
                                        on: sock.on,
                                    },
                                    &mut out,
                                );
                            }
                        }
                        return true;
                    }
                }
                false
            })
            .count();
        let total = qu.len() + cut;
        if last != total {
            highwatermark = std::cmp::max(highwatermark, total);
            evt(
                Event::Sent {
                    connected: qu.len(),
                    disconnected: cut,
                    highwatermark,
                    at: chrono::Utc::now(),
                },
                &mut out,
            );
            last = total;
        }
        out.flush()?;
        std::thread::sleep(Duration::from_secs(1));
    }
}
